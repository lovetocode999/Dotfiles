#----- My ZSH Config -----#

# Stuff that has to be done before anything else {{{

## Enable Profiling
if [[ $ZPROF = true ]]; then
    zmodload zsh/zprof
fi

## Don't do anything if not running interactively 
case $- in
    *i*) ;;
      *) return;;
esac

## Source .profile before anything else so important variables (e.g., $PATH)
## are available for programs when needed
source ~/.profile

## Add functions to fpath
fpath=($fpath $ZDOTDIR/functions)

## Autostart sway if running on TTY1
if test -z "${XDG_RUNTIME_DIR}"; then
    export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir
    if ! test -d "${XDG_RUNTIME_DIR}"; then
        mkdir "${XDG_RUNTIME_DIR}"
        chmod 0700 "${XDG_RUNTIME_DIR}"
    fi
fi
if [[ -z $DISPLAY ]] && [[ $TTY = /dev/tty1 ]]; then exec sway; fi

# }}}

# Variables {{{

## Random number for current ZSH session
SESSIONNUM=$RANDOM
export SESSIONNUM

# }}}

# Aliases {{{

## config alias to keep track of my dot files
alias config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"

## Alias ls to exa
alias ls="exa --icons --all"

## Alias lsg to exa git
alias lsg="exa --icons --long --grid --git --all"

## Alias less to make it read escape codes
alias less="less --chop-long-lines --RAW-CONTROL-CHARS"

## Connect to ctrl-c.club
alias ccc="mosh ctrl-c.club"

## Tic-tac toe online multi player
alias tictactoe="telnet pixelomer.com"

## Play tron in the terminal!
alias sshtron="ssh sshtron.zachlatta.com"

## Watson aliases
## Watson Report
alias wr="watson report"

# }}}

# Functions {{{

## Function to do mkdir and cd in the same command
autoload -Uz __mkcdir
alias mkcdir="__mkcdir"

## Function to do z and ls in the same command
autoload -Uz __lz
alias lz="__lz"

## Function to do cd and ls in the same command
autoload -Uz __lscd
alias lcd="__lscd"

## Handy function for scanning stuff
autoload -Uz __printscan
alias scan="__printscan"

## Calculator
autoload -Uz __calc
alias calc="noglob __calc"

## Kitty tab title
function set-title-precmd() {
    printf "\e]2;%s\a" "${PWD/#$HOME/~}"
}
function set-title-preexec() {
    printf "\e]2;%s\a" "$1"
}
autoload -Uz add-zsh-hook
add-zsh-hook precmd set-title-precmd
add-zsh-hook preexec set-title-preexec

## Bar cursor in-between commands
function ins_cursor() {
    print -n '\033[5 q'
}
add-zsh-hook preexec ins_cursor

# }}}

# Plugins {{{

## Fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

## Zinit
### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi
source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
### End of Zinit's installer chunk

## Reminders
zinit from"gl" wait lucid for \
    "lovetocode999/zsh-reminder"

## Man pages for zinit plugins
zinit wait lucid for \
    "zinit-zsh/z-a-man"

# Node Version Manager
zinit wait lucid for \
    "lukechilds/zsh-nvm"

## Colored man pages
zinit wait lucid for \
    "ael-code/zsh-colored-man-pages"

## Copy and paste files
zinit wait lucid for \
    "ChrisPenner/copy-pasta"

## Vi mode registers
zinit wait lucid for \
    "zsh-vi-more/evil-registers"

## Vi mode increment and decrement
zinit wait lucid for \
    "zsh-vi-more/vi-increment"

## Z for faster navigation
zinit wait lucid for \
    "agkozak/zsh-z"

## ZSH autocomplete
zinit wait lucid for \
    "marlonrichert/zsh-autocomplete"

## ZSH autopair
zinit wait lucid for \
    "hlissner/zsh-autopair"

## Fish like syntax highlighting
zinit wait lucid for \
     atload"ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay" \
        zdharma/fast-syntax-highlighting \
     blockf atload" aliases[=]='noglob __calc'; fast-theme zdharma > /dev/null" \
        zsh-users/zsh-completions \
     

## Starship Prompt theme
eval $(starship init zsh)

## Cursor shape (has to be run after starship because starship defines its own
## functions that conflict)
function zle-keymap-select zle-line-init zle-line-finish
{
    starship_render
    case $KEYMAP in
      vicmd)      print -n '\033[1 q';; # block cursor
      viins|main) print -n '\033[5 q';; # line cursor
    esac
    zle reset-prompt
}
zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select

# }}}

# ZSH settings {{{

## Bar as default cursor
print -n '\033[5 q'

## Instant escape key!
KEYTIMEOUT=1

## History settings
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

## Automatically cd when a directory is entered on its own and error on no glob match
setopt autocd nomatch

## Don't beep or notify
unsetopt beep notify

## Vi like key binds
bindkey -v

## Completions
zstyle :compinstall filename "$ZDOTDIR/.zshrc"

## Interactive comments
setopt interactivecomments

# }}}

# Finishing {{{

## Finish profiling
if [[ $ZPROF = true ]]; then
    zprof
fi

# }}}

# vim:foldmethod=marker:foldlevel=0
