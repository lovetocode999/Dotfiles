// ==UserScript==
// @name            Name
// @author          lovetocode999
// @namespace       https://lovetocode999.github.io/
// @description     Description
// @license         MIT License
// @version	        0.1
// @include         /^https?:\/\/www\.example\.org\/.*/
// @released        YYYY-MM-DD
// @updated         YYYY-MM-DD
// @compatible      Greasemonkey
// @run-at          document-end
// ==/UserScript==
 
(function(){
 
    function main(){
    };
 
    // instantiate and run 
    var main = new main();
})();
