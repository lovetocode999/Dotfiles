" HERE BE DRAGONS. USE AT YOUR OWN RISK!!!

" Vim plug {{{

    call plug#begin()

        " Emmet is a plugin for easy HTML writing
        Plug 'mattn/emmet-vim', { 'for': ['html', 'css', 'xml'] }

        " Vim polyglot is a language pack, quite handy when you're editing a
        " filetype that vim doesn't natively support 😉
        Plug 'sheerun/vim-polyglot'

        " Latex stuff, because I like to do math homework in Vim
        Plug 'lervag/vimtex', { 'for': 'tex' }
        Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }

        " FZF!!!! FUZZY SEARCHING WILL RULE THE WORLD!!!!!!!
        " MUAHAHAHAHA!!!!!!!
        Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
        Plug 'junegunn/fzf.vim'

        " Vim devicons, this is nothing but eye candy
        Plug 'ryanoasis/vim-devicons'

        " Because I sometimes edit javascript in HTML
        Plug 'AndrewRadev/inline_edit.vim'

        " Markdown stuff, mainly for READMEs on GitHub
        Plug 'gabrielelana/vim-markdown', { 'for': 'markdown' }
        Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install', 'for': 'markdown' }

        " Vim Obsession (Yes, I really am obsessed with VIM)
        Plug 'tpope/vim-obsession'

        " Custom startscreen - YAY!!
        Plug 'mhinz/vim-startify'

        " It's not alchohol, don't worry. If you don't believe me google
        " 'ale vim' and be amazed
        Plug 'dense-analysis/ale'

        " Coc is short for coconut. Just kidding, coc.nvim is for snippets and
        " autocomplete
        Plug 'neoclide/coc.nvim', {'branch': 'release'}

        " NERDTree. Yep, I even browse files in Vim
        Plug 'preservim/nerdtree'
        Plug 'Xuyuanp/nerdtree-git-plugin'

        " Abolish.vim - I mostly use this for coercion, because sometimes I
        " want to change variable name formats
        Plug 'tpope/vim-abolish'

        " Surround.vim - huzzah for surround!!!
        Plug 'tpope/vim-surround'

        " Repeat.vim
        Plug 'tpope/vim-repeat'

        " Auto-pairs - because it's handy for writing code
        Plug 'jiangmiao/auto-pairs'

        " Whole bunch of stuff for writing in vim: Goyo, limelight, and pencil
        Plug 'junegunn/goyo.vim', { 'for': ['markdown', 'tex', 'text', 'mail'] }
        Plug 'junegunn/limelight.vim', { 'for': ['markdown', 'tex', 'text', 'mail'] }
        Plug 'reedes/vim-pencil', { 'for': ['markdown', 'tex', 'text', 'mail'] }
        Plug 'dbmrq/vim-ditto', { 'for': ['markdown', 'tex', 'text', 'mail'] }

        " Coc completion for ZSH
        Plug 'tjdevries/coc-zsh'

        " Coc completion for Neovim's built-in LSP
        Plug 'rafcamlet/coc-nvim-lua'

        " Highlight Löve2d keywords
        Plug 'davisdude/vim-love-docs', { 'branch': 'build' }

        " 6502 Assembly
        Plug 'maxbane/vim-asm_ca65'

        " Preview hex colors
        Plug 'chrisbra/Colorizer'

        " Python code folding
        Plug 'tmhedberg/SimpylFold', { 'for': 'python' }

        " Sway config highlighting
        Plug 'terminalnode/sway-vim-syntax'

        " Vim-kitty navigator for consistent split keybindings
        Plug 'knubie/vim-kitty-navigator'

        " Easy access to system commands
        Plug 'tpope/vim-eunuch'

        " Gruvbox color scheme
        Plug 'morhetz/gruvbox'

        " Highlight the yanked portion of text
        Plug 'machakann/vim-highlightedyank'

        " Buffers as tabs
        Plug 'zefei/vim-wintabs'

        " Org Mode
        Plug 'jceb/vim-orgmode'

        " SLIMV (Superior Lisp Interaction Mode for Vim)
        Plug 'kovisoft/slimv'

        " Rainbow (Great for lisp development!)
        Plug 'luochen1990/rainbow'

    call plug#end()

" }}}

" Mappings {{{

" Langmap {{{
    set langmap=qq,dw,re,wr,bt,jy,fu,ui,po,\\;p,aa,ss,hd,tf,gg,yh,nj,ek,ol,i\\;,zz,xx,mc,cv,vb,kn,lm,QQ,DW,RE,WR,BT,JY,FU,UI,PO,:P,AA,SS,HD,TF,GG,YH,NJ,EK,OL,I:,ZZ,XX,MC,CV,VB,KN,LM
" }}}

" Normal mappings {{{

    " Set leader to , for easier bindings
    let mapleader=","

    " Unhighlight search matches when escape is pressed
    nnoremap <silent> <esc> :nohlsearch<cr>

    " Easy window navigation bindings
    nnoremap <C-N> <C-W><C-J>
    nnoremap <C-E> <C-W><C-K>
    nnoremap <C-O> <C-W><C-L>
    nnoremap <C-Y> <C-W><C-H>

    " Custom mappings for vim-kitty-navigator
    nnoremap <silent> <c-y> :KittyNavigateLeft<cr>
    nnoremap <silent> <c-n> :KittyNavigateDown<cr>
    nnoremap <silent> <c-e> :KittyNavigateUp<cr>
    nnoremap <silent> <c-o> :KittyNavigateRight<cr>

    " Easy split bindings
    let g:vert=0
    nnoremap <silent> vv @=':let g:vert=1 \| vnew'<CR><CR>
    nnoremap <silent> vh @=':let g:vert=2 \| new'<CR><CR>

    " Easy binding to open/close folds
    nmap <space> za

    " Binding to open NERDTree
    nmap <silent> <C-f> :NERDTreeToggle<Return>

    " Toggle spell checking
    noremap <silent> <leader>s :set spell!<CR>

    " Toggle folding
    noremap <silent> <leader>f :call <SID>foldtoggle()<CR>

    " Call Markdown preview
    noremap <leader>m <Plug>MarkdownPreview

    " Easier Buffer Mappings
    nmap J <Plug>(wintabs_previous)
    nmap K <Plug>(wintabs_next)
    noremap gl :WintabsMove -1<CR>
    noremap gr :WintabsMove 1<CR>

" }}}

" Terminal mappings {{{

    " Easier way to get out of terminal mode
    tnoremap fj <C-\><C-n>

    " <M-hjkl> as arrows
    tmap <M-h> <Left>
    tmap <M-j> <Down>
    tmap <M-k> <Up>
    tmap <M-l> <Right>

" }}}

" Visual Mappings {{{

    " Yank without moving the cursor
    vnoremap <expr>y "my\"" . v:register . "y`y"

" }}}

" Insert Mappings {{{

    " use <tab> for trigger completion and navigate to the next complete item
    inoremap <silent><expr> <Tab>
                \ pumvisible() ? "\<C-n>" :
                \ <SID>check_back_space() ? "\<Tab>" :
                \ coc#refresh()

    " Enter to confirm completion
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

    " S-Tab to navigate coc completion list
    inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

    " <M-hjkl> as arrows
    map! <M-h> <Left>
    map! <M-j> <Down>
    map! <M-k> <Up>
    map! <M-l> <Right>

    " Break my bad habit of over-using <BS>
    imap <BS> <Nop>

" }}}

" Command mappings {{{
    
    " Make buffer/window commands use Wintabs {{{
    " Next buffer
    cabbrev <silent> bn WintabsNext
    cabbrev <silent> bn! WintabsNext
    cabbrev <silent> bnext WintabsNext
    cabbrev <silent> bnext! WintabsNext
    " Previous buffer
    cabbrev <silent> bp WintabsPrevious
    cabbrev <silent> bp! WintabsPrevious
    cabbrev <silent> bprevious WintabsPrevious
    cabbrev <silent> bprevious! WintabsPrevious
    " Delete buffer
    cabbrev <silent> bd WintabsClose
    cabbrev <silent> bdelete WintabsClose
    " Undo previous buffer/tab/window action
    cabbrev <silent> bund WintabsUndo
    cabbrev <silent> bund! WintabsUndo
    cabbrev <silent> bundo WintabsUndo
    cabbrev <silent> bundo! WintabsUndo
    " Close all but the current wintab
    cabbrev <silent> bon WintabsOnly
    cabbrev <silent> bon! WintabsOnly
    cabbrev <silent> bonly WintabsOnly
    cabbrev <silent> bonly! WintabsOnly
    " Close the current window
    cabbrev <silent> clo WintabsCloseWindow
    cabbrev <silent> close WintabsCloseWindow
    cabbrev <silent> clo! WintabsCloseWindow
    cabbrev <silent> close! WintabsCloseWindow
    " Close all but the current window
    cabbrev <silent> onl WintabsOnlyWindow
    cabbrev <silent> onl! WintabsOnlyWindow
    cabbrev <silent> only WintabsOnlyWindow
    cabbrev <silent> only! WintabsOnlyWindow
    " Close the current vimtab
    cabbrev <silent> tabc WintabsCloseVimtab
    cabbrev <silent> tabc! WintabsCloseVimtab
    cabbrev <silent> tabclose WintabsCloseVimtab
    cabbrev <silent> tabclose! WintabsCloseVimtab
    " Close all but the current vimtab
    cabbrev <silent> tabo WintabsOnlyVimtab
    cabbrev <silent> tabo! WintabsOnlyVimtab
    cabbrev <silent> tabonly WintabsOnlyVimtab
    cabbrev <silent> tabonly! WintabsOnlyVimtab
    " }}}

" }}}

" Multi-mode mappings {{{

    " Swap ; and :
    noremap ; :
    noremap : ;

    " <M-hjkl> as arrows
    map <M-h> <Left>
    map <M-j> <Down>
    map <M-k> <Up>
    map <M-l> <Right>

" }}}

" 

" }}}

" Settings {{{

" Functional Aesthetics {{{

    " Logical splits
    set splitbelow
    set splitright

    " Make Vim able to read and display UTF-8 files
    set encoding=UTF-8

    " Tabs are 4 spaces
    set tabstop=4
    set shiftwidth=4
    set expandtab

    " Highlight search results
    set hlsearch

    " Hybrid line numbers
    set nu rnu

    " Highlight incorrectly spelled words
    set spell

    " Hightlight the line the cursor is on
    set cursorline

    " Enable folds
    set foldenable
    
    " I have different settings for this in the modelines of most of my files,
    " so it is not a good idea to run this if the init.vim file has already
    " been executed
    if exists('s:vimsourced')
        " Only fold very nested code and limit fold nesting
        set foldlevelstart=10
        set foldnestmax=10

        " Fold based on indents in code
        set foldmethod=indent
        let s:vimsourced = 1
    endif

    " Scrolling
    set sidescroll=1
    set scrolloff=5
    set sidescrolloff=5

    " Confirmation message when quitting
    set confirm

    " Don't pass messages to completion
    set shortmess+=c

    " Don't wrap
    set nowrap

    " More colors in true-color terminals
    set termguicolors

    " Don't open folds while searching
    set foldopen-=search

    " Interactive substitution
    set inccommand=split

    " Terminal title
    set title
    let &titlestring='%t - nvim'

" }}}

" Functional settings {{{

    " Reload file if it is changed externally
    set autoread

    " Reasonable case sensitivity for searching
    set ignorecase
    set smartcase

    " Hide buffers without unloading them
    set hidden

    " Redraw when needed instead of every frame (Why isn't this the
    " default?!?)
    set lazyredraw

    " Instant escape key :D
    set timeoutlen=1000 ttimeoutlen=0

    " Backup and undofile
    set backup
    set backupdir=$HOME/.config/nvim/backups
    set directory=$HOME/.config/nvim/backups
    set writebackup
    set undofile

" }}}

" Useless variables {{{

    " π. Should anything else be said? It's π. 
    " Extra comment line so it ends up on line 314 😉
    let pi=3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823

" }}}

" Plugin settings {{{

    " Set latex live preview to use termpdf.py in a kitty split
    let g:livepreview_previewer = 'kitty @ new-window termpdf.py'

    " Tex is latex. BAM!
    let g:tex_flavor = 'latex'

    " Show hidden files in NERDTree
    let NERDTreeShowHidden=1

    " Make markdown preview use qutebrowser
    let g:mkdp_browser = 'qutebrowser'

    " Vim startify options {{{

        let g:startify_bookmarks = [ {'c': '$HOME/.config/nvim/init.vim'}, {'z': '$HOME/.config/zsh/.zshrc'}]
        let g:startify_lists = [
              \ { 'type': 'bookmarks', 'header': ['   Bookmarks']              },
              \ { 'type': 'sessions',  'header': ['   Sessions']               },
              \ { 'type': 'files',     'header': ['   Files']                  },
              \ { 'type': 'commands',  'header': ['   Commands']               },
              \ ]
        let g:startify_fortune_use_unicode = 1
        let g:startify_enable_special = 0
        let g:startify_session_savevars = [
                    \ 'g:startify_session_savevars',
                    \ 'g:startify_session_savecmds',
                    \ ]
        let g:startify_session_number = 999
        let g:startify_session_sort = 1
        let g:startify_custom_indices = ['a', 's', 'd', 'f', 'h', 'l']
        let g:ascii = [
                    \ '         //                 /*          ',
                    \ '      ,(/(//,               *###        ',
                    \ '    ((((((////.             /####%*     ',
                    \ ' ,/(((((((/////*            /########   ',
                    \ '/*///((((((//////.          *#########/ ',
                    \ '//////((((((((((((/         *#########/.',
                    \ '////////((((((((((((*       *#########/.',
                    \ '/////////(/(((((((((((      *#########(.',
                    \ '//////////.,((((((((((/(    *#########(.',
                    \ '//////////.  /(((((((((((,  *#########(.',
                    \ '(////////(.    (((((((((((( *#########(.',
                    \ '(////////(.     ,#((((((((((##########(.',
                    \ '((//////((.       /#((((((((##%%######(.',
                    \ '((((((((((.         #(((((((####%%##%#(.',
                    \ '((((((((((.          ,((((((#####%%%%%(.',
                    \ ' .#(((((((.            (((((#######%%   ',
                    \ '    /(((((.             .(((#%##%%/*    ',
                    \ '      ,(((.               /(#%%#        ',
                    \ '        ./.                 #*          ',
                    \ '                                        '
                    \]
        let g:startify_custom_header = g:ascii + startify#fortune#boxed()
        let g:footer_ascii = ['--  The one true editor, NeoVim  --']
        let g:startify_custom_footer = g:footer_ascii
        let g:startify_session_persistence = 1

    " }}}

    " Use NERDTree as a file explorer instead of Netrw
    let NERDTreeHijackNetrw = 1

    " Ale signs 🍺
    let g:ale_sign_error = '❌'
    let g:ale_sign_warning = '⚠️'

    " Easy prefix for Emmet expansion
    let g:user_emmet_leader_key=','

    " Auto pairs ) to insert )
    let g:AutoPairsShortcutBackInsert = '<C-Space>'

    " Auto pairs no <BS>
    let g:AutoPairsMapBS = 0

    " Make limelight work
    let g:limelight_conceal_ctermfg = 'gray'
    let g:limelight_conceal_ctermfg = 240

    " Use tab key to complete snippets
    let g:coc_snippet_next = '<Tab>'

    " Use S-Tab to go back in snippet placeholders
    let g:coc_snippet_prev = '<S-Tab>'

    " Enable italics in gruvbox
    let g:gruvbox_italic = 1

    " Soft dark gruvbox
    let g:gruvbox_contrast_dark = 'medium'

    " No background for the sign column (to the left of the numbers)
    let g:gruvbox_sign_column = 'bg0'

    " Better strings
    let g:gruvbox_improved_strings = 1

    " Highlight compiler warnings
    let g:gruvbox_improved_warnings = 1

" }}}

" }}}

" Autocmds {{{

    " Put these in an autocmd group, so that we can delete them easily.
    augroup vimrcEx

        au!

        " For all gemini files set filetype to gmi
        autocmd BufRead,BufNewFile *.gmi,*.gemini set filetype=gmi

        " For all text files set 'textwidth' to 80 characters.
        autocmd FileType text setlocal textwidth=80

        " Automatically fix python files
        autocmd FileType python let b:ale_fixers = ['autopep8'] | let b:ale_linters = ['flake8']

        " Make vimrc have no spell checking
        autocmd FileType vim set nospell

        " Rainbow for lisp
        autocmd FileType lisp RainbowToggleOn

        " Reload picom when editing picom.conf
        autocmd BufWritePost picom.conf silent! !picom -b

        " Template for MLA latex files
        autocmd BufNewFile *.mla.tex call <SID>mlatex()
        " Template for latex files
        augroup latex_normal
            autocmd BufNewFile *.tex call <SID>normtex()
        augroup END
        
        " Template for greasemonkey files
        autocmd BufNewFile *.gm.js 0r $HOME/.config/nvim/templates/skeleton.gm.js

        " Set the colorscheme to Gruvbox - this has to be done immediately
        " before setting the cursorline in vim-startify
        colo gruvbox
        hi VertSplit guifg=fg guibg=bg

        " Set cursorline in startify
        autocmd User Startified set cursorline

        " Start other plugins when starting Goyo
        autocmd! User GoyoEnter nested call <SID>goyo_enter()
        autocmd! User GoyoLeave nested call <SID>goyo_leave()

        " Autostart Goyo
        autocmd BufEnter * call <SID>auto_goyo()

        " Email specific formatting
        autocmd FileType mail call <SID>email_format()

        " Syntax highlighting for json comments
        autocmd FileType json syntax match Comment +\/\/.\+$+

        " Wiz syntax highlighting
        autocmd BufRead,BufNewFile *.wiz set filetype=wiz
        
        " (La)Tex specific bindings
        autocmd FileType tex inoremap / <Bslash>| inoremap // /

        " Ctags
        autocmd BufEnter * call <SID>gen_ctags()

    augroup END

" }}}

" Functions {{{

    " Function to load the mla.tex template
    function s:mlatex() 
        " Load skeleton file
        0r $HOME/.config/nvim/templates/skeleton.mla.tex 

        " Save typeahead and clear it to avoid errors
        call inputsave() 

        " Prompt for teacher's name
        let teachname = substitute(input('Teacher: '), '\. ', '.\\ ', 'g')
        " Prompt for class name
        let classname = substitute(input('Class: '), '\. ', '.\\ ', 'g')
        " Prompt for paper title
        let papername = substitute(input('Title: '), '\. ', '.\\ ', 'g')

        " Restore typeahead
        call inputrestore()

        " Current date
        let curdate = system('date +%d\ %B\ %Y')
        let curdate = substitute(curdate, '\n', '', 'g')

        " Insert the teacher's name into the skeleton file
        %substitute/${teachname}/\=teachname/
        " Insert the class name into the skeleton file
        %substitute/${classname}/\=classname/
        " Insert the date into the skeleton file
        %substitute/${curdate}/\=curdate/ 
        " Insert the paper title into the skeleton file
        %substitute/${papername}/\=papername/ 
        autocmd! latex_normal BufNewFile *.tex
        LLPStartPreview
    endfunction

    " Function to load the normal .tex template
    function s:normtex()
        " Load skeleton file
        0r $HOME/.config/nvim/templates/skeleton.tex

        " Save typeahead and clear it to avoid errors
        call inputsave()

        " Prompt for document title
        let doctitle = substitute(input('Title: '), '\. ', '.\\ ', 'g')

        " Restore typeahead
        call inputrestore()

        " Current date
        let curdate = system('date +%d\ %B\ %Y')
        let curdate = substitute(curdate, '\n', '', '')

        " Insert the document title into the skeleton file
        %substitute/${doctitle}/\=doctitle/
        " Insert the date into the skeleton file
        %substitute/${curdate}/\=curdate/
        LLPStartPreview
    endfunction

    " Function to toggle folds
    function s:foldtoggle()
        if &foldlevel == 0
            set foldlevel=10
        else
            set foldlevel=0
        endif
    endfunction

    " Function to check if the backspace key has been pressed
    function! s:check_back_space() abort
        let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~ '\s'
    endfunction

    " Function to make devicons work in startify
    function! StartifyEntryFormat()
        return '." (". WebDevIconsGetFileTypeSymbol(absolute_path) .") ". entry_path '
    endfunction

    " Functions to toggle multiple plugins when using Goyo
    function! s:goyo_enter()
        SoftPencil
        Limelight
        DittoOn
        Ditto
        set noshowmode
        set noshowcmd
        set scrolloff=999
        set eventignore=FocusGained
        nmap q :qa<Enter>
        let b:quitting = 0
        let b:quitting_bang = 0
        autocmd QuitPre <buffer> silent! let b:quitting = 1
        cabbrev <silent> <buffer> q! let b:quitting_bang = 1 <bar> q!
    endfunction
    function! s:goyo_leave() 
        NoPencil
        Limelight!
        DittoOff
        set showmode
        set showcmd
        set scrolloff=5
        set eventignore=
        nunmap q
        " Quit Vim if this is the only remaining buffer
        if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
            if b:quitting_bang
                qa!
            else
                qa
            endif
        endif
    endfunction

    " Function to generate ctags file
    function! s:gen_ctags()
        if &ft == 'lua'
            silent !ctags -R . &
        endif
    endfunction

    " Function to automatically start goyo
    function! s:auto_goyo()
        if &ft == 'markdown' || &ft == 'text' || &ft == 'tex' || &ft == 'mail'
            if !exists("b:quitting")
                Goyo 80
            endif
        endif
    endfunction

    " Function to make writing emails easier
    function! s:email_format()
        set foldmethod=syntax
        set foldlevel=0
        set hidden
    endfunction

" }}}

" vim:foldmethod=marker:foldlevel=0
